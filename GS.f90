subroutine gauss_seidel() 
    use variables
    implicit none



    ik=0
    pChangeMax = 1.0
    pChangeMax_= 1.0

    do while (pChangeMax>zeta .AND. ik < itmax)

    ik=ik+1
    pChangeMax = 0.0

        !----------data transformation among nodes----------! nz*nx(non-continue) B_nbr, T_nbr
        icount = ((i_jpend-i_jpstart)+1)

        call MPI_TYPE_VECTOR(icount, nx+4, (ny+4)*(nx+4), MPI_REAL8, VECT_nznx, ierr) 
        call MPI_TYPE_COMMIT(VECT_nznx, ierr) 

        itag = 370
        call MPI_SENDRECV( p(-1,i_ipend,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                           p(-1,i_ipstart-1,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )  

        itag = 380
        call MPI_SENDRECV( p(-1,i_ipstart,i_jpstart), 1, VECT_nznx, B_nbr, itag, &
                           p(-1,i_ipend+1,i_jpstart), 1, VECT_nznx, T_nbr, itag, comm2D, status, ierr )
                    
        call MPI_BARRIER(comm2D, ierr)
        !----------data transformation among nodes----------! nz*nx(non-continue) B_nbr, T_nbr


        !----------data transformation among nodes----------! ny*nx(continue) R_nbr, L_nbr
        icount = (nx+4)*((i_ipend-i_ipstart)+1)     
        itag = 390
        call MPI_SENDRECV( p(-1,i_ipstart,i_jpend), icount, MPI_REAL8, r_nbr, itag, &
                           p(-1,i_ipstart,i_jpstart-1), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )

        itag = 400
        call MPI_SENDRECV( p(-1,i_ipstart,i_jpstart), icount, MPI_REAL8, l_nbr, itag, &
                           p(-1,i_ipstart,i_jpend+1), icount, MPI_REAL8, r_nbr, itag, comm2D, status, ierr )

        call MPI_BARRIER(comm2D, ierr)
        !----------data transformation among nodes----------! ny*nx(continue) R_nbr, L_nbr



        do k=i_jpstart,i_jpend
        !$OMP PARALLEL DO PRIVATE(i,mChangeMax,mChange,pNEW) 
        do j=i_ipstart,i_ipend
        do i=1,nx

            mChange = ( u_star(i,j,k) - u_star(i-1,j,k) ) * iDy(j) * iDz(k) &
                    + ( v_star(i,j,k) - v_star(i,j-1,k) ) * iDx(i) * iDz(k) &
                    + ( w_star(i,j,k) - w_star(i,j,k-1) ) * iDx(i) * iDy(j)

            pNEW = (- p(i+1,j,k) * iDy(j) * iDz(k) / Dxs(i) &
                    - p(i-1,j,k) * iDy(j) * iDz(k) / Dxs(i-1) &
                    - p(i,j+1,k) * iDx(i) * iDz(k) / Dys(j) &
                    - p(i,j-1,k) * iDx(i) * iDz(k) / Dys(j-1) &
                    - p(i,j,k+1) * iDx(i) * iDy(j) / Dzs(k) &
                    - p(i,j,k-1) * iDx(i) * iDy(j) / Dzs(k-1) &
                    + mChange / dt) /  (- iDy(j) * iDz(k) / Dxs(i) - iDy(j) * iDz(k) / Dxs(i-1) &
                                        - iDx(i) * iDz(k) / Dys(j) - iDx(i) * iDz(k) / Dys(j-1) &
                                        - iDx(i) * iDy(j) / Dzs(k) - iDx(i) * iDy(j) / Dzs(k-1) )
            
            pChange = abs(pNew - P(i,j,k))

            P(i,j,k) = P(i,j,k) + ( omega * (pNew - P(i,j,k)) )

            !if (abs(mChange) > mChangeMax) then
            !    mChangeMax = abs(mChange)
            !end if

            if (pChange > pChangeMax) then
                pChangeMax = pChange
            end if

             
        end do
        end do
        !$OMP END PARALLEL DO
        end do
        
        

        !do k=i_jpstart,i_jpend; 
        !!$OMP PARALLEL DO PRIVATE(i,mChangeMax,mChange) 
!
        !do j=i_ipstart,i_ipend;
        !do i=MOD(j+k,2)+1,nx,2
!
        !    mChange = ( u_star(i,j,k) - u_star(i-1,j,k) ) * iDy(j) * iDz(k) &
        !            + ( v_star(i,j,k) - v_star(i,j-1,k) ) * iDx(i) * iDz(k) &
        !            + ( w_star(i,j,k) - w_star(i,j,k-1) ) * iDx(i) * iDy(j)
!
        !    pNEW = (- p(i+1,j,k) * iDy(j) * iDz(k) / Dxs(i) &
        !            - p(i-1,j,k) * iDy(j) * iDz(k) / Dxs(i-1) &
        !            - p(i,j+1,k) * iDx(i) * iDz(k) / Dys(j) &
        !            - p(i,j-1,k) * iDx(i) * iDz(k) / Dys(j-1) &
        !            - p(i,j,k+1) * iDx(i) * iDy(j) / Dzs(k) &
        !            - p(i,j,k-1) * iDx(i) * iDy(j) / Dzs(k-1) &
        !            + mChange / dt) /  (- iDy(j) * iDz(k) / Dxs(i) - iDy(j) * iDz(k) / Dxs(i-1) &
        !                                - iDx(i) * iDz(k) / Dys(j) - iDx(i) * iDz(k) / Dys(j-1) &
        !                                - iDx(i) * iDy(j) / Dzs(k) - iDx(i) * iDy(j) / Dzs(k-1) )
        !    
        !    pChange = abs(pNew - P(i,j,k))
!
        !    P(i,j,k) = P(i,j,k) + ( omega * (pNew - P(i,j,k)) )
!
        !    if (abs(mChange) > mChangeMax) then
        !        mChangeMax = abs(mChange)
        !    end if
!
        !    if (pChange > pChangeMax) then
        !        pChangeMax = pChange
        !    end if
!
        !     
        !end do
        !end do; 
        !!$OMP END PARALLEL DO
!
        !end do
!
!
!
        !do k=i_jpstart,i_jpend; 
        !!$OMP PARALLEL DO PRIVATE(i,mChangeMax,mChange) 
!
        !do j=i_ipstart,i_ipend;
        !do i=MOD(J+k+1,2)+1,nx,2
!
        !    mChange = ( u_star(i,j,k) - u_star(i-1,j,k) ) * iDy(j) * iDz(k) &
        !            + ( v_star(i,j,k) - v_star(i,j-1,k) ) * iDx(i) * iDz(k) &
        !            + ( w_star(i,j,k) - w_star(i,j,k-1) ) * iDx(i) * iDy(j)
!
        !    pNEW = (- p(i+1,j,k) * iDy(j) * iDz(k) / Dxs(i) &
        !            - p(i-1,j,k) * iDy(j) * iDz(k) / Dxs(i-1) &
        !            - p(i,j+1,k) * iDx(i) * iDz(k) / Dys(j) &
        !            - p(i,j-1,k) * iDx(i) * iDz(k) / Dys(j-1) &
        !            - p(i,j,k+1) * iDx(i) * iDy(j) / Dzs(k) &
        !            - p(i,j,k-1) * iDx(i) * iDy(j) / Dzs(k-1) &
        !            + mChange / dt) /  (- iDy(j) * iDz(k) / Dxs(i) - iDy(j) * iDz(k) / Dxs(i-1) &
        !                                - iDx(i) * iDz(k) / Dys(j) - iDx(i) * iDz(k) / Dys(j-1) &
        !                                - iDx(i) * iDy(j) / Dzs(k) - iDx(i) * iDy(j) / Dzs(k-1) )
        !    
        !    
!
        !    pChange = abs(pNew - P(i,j,k))
!
        !    P(i,j,k) = P(i,j,k) + ( omega * (pNew - P(i,j,k)) )
!
        !    if (abs(mChange) > mChangeMax) then
        !        mChangeMax = abs(mChange)
        !    end if
!
        !    if (pChange > pChangeMax) then
        !        pChangeMax = pChange
        !    end if
!
        !     
        !end do
        !end do; 
        !!$OMP END PARALLEL DO
!
        !end do

        






        
        

        call MPI_ALLREDUCE( pChangeMax, pChangeMax_, 1, MPI_REAL8, MPI_MAX, COMM2D, ierr )
        pChangeMax = pChangeMax_

        !if(mycid==master)then
        !    write(*,*) pChangeMax
        !end if
    
    end do









        do k=i_jpstart,i_jpend; 
        !$OMP PARALLEL DO PRIVATE(i) 

        do j=i_ipstart,i_ipend
        do i=1,nx

        pre(i,j,k) = p(i,j,k)

        end do
        end do; 
        !$OMP END PARALLEL DO
        end do




    if(myid==master)then

        
            print*, '          '
            print*, 'Iterations Gauss Seidel =',ik
        

    end if


end subroutine gauss_seidel
