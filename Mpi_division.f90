subroutine Mpi_division()
use variables 
use mpi
implicit none

    ! j (ip) ipart(2) M
    !
    ! M = 30 (j), N = 40 (k)
    ! nz*nx(non-continue)
    !
    !           !           !
    !   CPU2    !   CPU5    !   CPU8
    !   (0,2)   !   (1,2)   !   (2,2)
    !--------------------------------------
    !           !           !
    !   CPU1    !   CPU4    !   CPU7
    !   (0,1)   !   (1,1)   !   (2,1)
    !--------------------------------------
    !           !           !
    !   CPU0    !   CPU3    !   CPU6
    !   (0,0)   !   (1,0)   !   (2,0)
    !-----------------------------------------------------> k (jp) ipart(1) N  ny*nx(continue)

    !------------------------ Define coord of MPI ------------------------!
    ipart(1) = jp
    ipart(2) = ip
    periods(1) = .FALSE.
    periods(2) = .FALSE.
    reorder = .TRUE.


    call MPI_CART_CREATE(MPI_COMM_WORLD, MPI_ndim, ipart, periods, reorder, comm2D, ierr)
    call MPI_COMM_RANK(comm2D, mycid, ierr)
    call MPI_CART_COORDS(comm2D, mycid, MPI_ndim, mycoord, ierr)

    sideways = 0
    updown = 1
    right = 1
    up = 1

    call MPI_CART_SHIFT(comm2D, sideways, right, L_nbr, R_nbr, ierr)
    call MPI_CART_SHIFT(comm2D, updown, up, B_nbr, T_nbr, ierr)


    !write(*,*) mycid, mycoord(1), mycoord(2)
    !------------------------ Define coord of MPI ------------------------!




    

    !--------------------------- MPI division ----------------------------!
    Zdv = nz / jp !40
    Ydv = ny / ip !30

    do i = 1, (nproc-1)
        jpstart(i) = 1 + i * (Zdv+1)
    end do


    ik = 0
    do k=0, (jp-1); do j=0, (ip-1)

        jpstart(ik) = 1 + k * Zdv
        jpend(ik) = jpstart(ik) + Zdv -1


        ipstart(ik) = 1 + j * Ydv
        ipend(ik) = ipstart(ik) + Ydv -1

        ik = ik + 1
    end do; end do
    !--------------------------- MPI division ----------------------------!


   







    

end subroutine Mpi_division

