subroutine calcul_new_velocity()
    use variables
    implicit none


    !-------------------------------------------------!
    !    Calculation of velocity field at t = dt*n+1  !
    !-------------------------------------------------!

   


    do k=i_jpstart,i_jpend
    !$OMP PARALLEL DO PRIVATE(i) 
    do j=i_ipstart,i_ipend; do i=1,nx

    u1(i,j,k) = u_star(i,j,k) - dt*(p(i+1,j,k)-p(i,j,k)) / Dxs(i)
    v1(i,j,k) = v_star(i,j,k) - dt*(p(i,j+1,k)-p(i,j,k)) / Dys(j)
    w1(i,j,k) = w_star(i,j,k) - dt*(p(i,j,k+1)-p(i,j,k)) / Dzs(k)

    end do; end do
    !$OMP END PARALLEL DO
    end do


    




    !-------------------------------------------------!
    !    Calculation of velocity field for DFIB       !
    !-------------------------------------------------!

    


    do k=i_jpstart,i_jpend
    !$OMP PARALLEL DO PRIVATE(i) 
    do j=i_ipstart,i_ipend; do i=1,nx

    u2(i,j,k) = ETA(i,j,k) *u_solid + (1-ETA(i,j,k)) * u1(i,j,k)
    FX(i,j,k) = (u2(i,j,k) - u1(i,j,k)) / dt

    v2(i,j,k) = ETA(i,j,k) *v_solid + (1-ETA(i,j,k)) * v1(i,j,k)
    FY(i,j,k) = (v2(i,j,k) - v1(i,j,k)) / dt

    w2(i,j,k) = ETA(i,j,k) *w_solid + (1-ETA(i,j,k)) * w1(i,j,k)
    FZ(i,j,k) = (w2(i,j,k) - w1(i,j,k)) / dt

    end do; end do
    !$OMP END PARALLEL DO
    end do


    


    !匯流虛擬力
    !----------data collect among nodes----------!

    icount = ((i_jpend-i_jpstart)+1)

    call MPI_TYPE_VECTOR(icount, (nx+4)*((i_ipend-i_ipstart)+1), (ny+4)*(nx+4), MPI_REAL8, VECT_nznynx, ierr) 
    call MPI_TYPE_COMMIT(VECT_nznynx, ierr) 


    !Send my results back to the master
    if(mycid>master)then

        
        itag = 430
        call MPI_SEND( FX(-1,i_ipstart,i_jpstart), 1, VECT_nznynx, master, itag, comm2D, ierr )

        itag = 440
        call MPI_SEND( FY(-1,i_ipstart,i_jpstart), 1, VECT_nznynx, master, itag, comm2D, ierr )

        itag = 450
        call MPI_SEND( FZ(-1,i_ipstart,i_jpstart), 1, VECT_nznynx, master, itag, comm2D, ierr )


    end if


    !Wait to receive results from each task
    if(mycid==master)then

        do i = 1, (nproc-1)

                itag = 430
                call MPI_RECV( FX(-1,ipstart(i),jpstart(i)), 1, VECT_nznynx, i, itag, comm2D, status, ierr )

                itag = 440
                call MPI_RECV( FY(-1,ipstart(i),jpstart(i)), 1, VECT_nznynx, i, itag, comm2D, status, ierr )
                
                itag = 450
                call MPI_RECV( FZ(-1,ipstart(i),jpstart(i)), 1, VECT_nznynx, i, itag, comm2D, status, ierr )

        end do


    end if


    icount = (nx+4)*(ny+4)*(nz+4)
    call MPI_BARRIER(comm2D, ierr)
    call MPI_BCAST( FX(-1,-1,-1), icount, MPI_REAL8, master, comm2D, ierr )
    call MPI_BARRIER(comm2D, ierr)
    call MPI_BCAST( FY(-1,-1,-1), icount, MPI_REAL8, master, comm2D, ierr )
    call MPI_BARRIER(comm2D, ierr)
    call MPI_BCAST( FZ(-1,-1,-1), icount, MPI_REAL8, master, comm2D, ierr )
    call MPI_BARRIER(comm2D, ierr)
    !----------data collect among nodes----------!

    
      


    !----------------------------------------------------------------------------------------!
    !           Centering variables to get matrix (1:nx,1:ny,1:nz) for Tecplot               !
    !----------------------------------------------------------------------------------------!


    !----------data transformation among nodes----------! nz*nx(non-continue) B_nbr, T_nbr
    icount = ((i_jpend-i_jpstart)+1)

    call MPI_TYPE_VECTOR(icount, nx+4, (ny+4)*(nx+4), MPI_REAL8, VECT_nznx, ierr) 
    call MPI_TYPE_COMMIT(VECT_nznx, ierr) 

    itag = 460
    call MPI_SENDRECV( v2(-1,i_ipend,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                       v2(-1,i_ipstart-1,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )

    call MPI_BARRIER(comm2D, ierr)
    !----------data transformation among nodes----------! nz*nx(non-continue) B_nbr, T_nbr


    !----------data transformation among nodes----------! ny*nx(continue) R_nbr, L_nbr
    icount = (nx+4)*((i_ipend-i_ipstart)+1)

    itag = 470
    call MPI_SENDRECV( w2(-1,i_ipstart,i_jpend), icount, MPI_REAL8, r_nbr, itag, &
                       w2(-1,i_ipstart,i_jpstart-1), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )

                
    call MPI_BARRIER(comm2D, ierr)
    !----------data transformation among nodes----------! ny*nx(continue) R_nbr, L_nbr

    do k=i_jpstart,i_jpend
    !$OMP PARALLEL DO PRIVATE(i) 
    do j=i_ipstart,i_ipend; do i=1,nx

    uc(i,j,k) = 0.5*(u2(i,j,k)+u2(i-1,j,k))
    vc(i,j,k) = 0.5*(v2(i,j,k)+v2(i,j-1,k))
    wc(i,j,k) = 0.5*(w2(i,j,k)+w2(i,j,k-1))

    end do; end do
    !$OMP END PARALLEL DO
    end do


end subroutine calcul_new_velocity










subroutine Updating_velocity()
    use variables
    implicit none
    !---------------------------------------------------------!
    !    loops to update the main u and v velocity fields     !
    !---------------------------------------------------------!

   


    do k=i_jpstart,i_jpend
    !$OMP PARALLEL DO PRIVATE(i) 
    do j=i_ipstart,i_ipend; do i=1,nx

        u(i,j,k) = u2(i,j,k)    
        v(i,j,k) = v2(i,j,k)  
        w(i,j,k) = w2(i,j,k) 

    end do; end do
    !$OMP END PARALLEL DO
    end do


 

end subroutine Updating_velocity


